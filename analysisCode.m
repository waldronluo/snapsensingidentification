%============================================================
% Not used exp for testing
% needed to train
function acc = analysisCode (rootPath, maxCases, seqNum)
%============================================================
% import used directories.
    %========================================================
    % Data load directory
    addpath('./load/');
    %========================================================
    % SVM library and svm data construct
    addpath('./svm/');

%============================================================
% Initialize the variables for successCases. failureCases, anaCases
% successCases: Matrix for success cases. Each row of successCases contains six elements: F/M signals in Fx, Fy, Fz, Mx, My, Mz axis.
% failureCases: Matrix for failure cases. Each row of failureCases contains six elements: F/M signals in Fx, Fy, Fz, Mx, My, Mz axis.
% experimentCases: Matrix for experiment cases. Like the two below.
% anaCases: The cases under rootPath.

    successCases = []; 
    failureCases = [];
    experimentCases = [];
    anaCases = ls(rootPath);

%============================================================
% Flags that needed to determine whether a training method should be used. All methods are used separately.
% svm_training: If svm should be used
% logistic_training: If logistic regression should be used. Not implemented.

%============================================================
% For svm training using data-driven approach.
    global svm_dataDriven_SxF;
    svm_dataDriven_SxF = 0;

        %====================================================
        % With only approach stage or the whole process?
        global svm_dataDriven_Approach;
        global svm_dataDriven_Whole;
        svm_dataDriven_Approach = 1;
        svm_dataDriven_Whole = 1; 

%============================================================
% For svm_training using labels.
    global svm_Fsub;
    global svm_SxF;
    svm_Fsub = 0;
    svm_SxF = 1;


        %====================================================
        % If we want to do it with whole process of the LLBs or some process of the LLBs
        % svm_LLB_whole: succeed. For the much more labels of succeessful cases.
        % svm_LLB_approach: failed. Labels are much the same.
        global svm_LLB_whole ;
        global svm_LLB_approach ;
        
        svm_LLB_whole = 1;
        svm_LLB_approach = 1;

        %====================================================
        % If we want to do it with whole process of the MCs
        % svm_MC_whole
        % svm_MC_approach
        global svm_MC_whole ;
        global svm_MC_approach ;

        svm_MC_whole = 1;
        svm_MC_approach = 1;

        %====================================================
        % If we want to do it with whole process of the Ps
        % svm_P_whole
        % svm_P_approach
        global svm_P_whole ;
        global svm_P_approach ;
        global svm_P_approach_process;

        svm_P_whole = 1;
        svm_P_approach = 1;
        svm_P_approach_process = 0; 

%============================================================
% acc : The accuracy array for each test.
    acc = [];

%============================================================
    if (length(who('seqNum')) == 0)
        seqNum = 1;
    end


%===========================================================
% Analysis with svm data-drivenly.

    if (svm_dataDriven_SxF)
        %===================================================
        if (svm_dataDriven_Approach)
            tmpAcc = svmPredict (rootPath, 'dataDriven');
            tmpAcc = [acc; tmpAcc];
        end
        if (svm_dataDriven_Whole)
            tmpAcc = svmPredict (rootPath, 'dataDriven');
            tmpAcc = [acc; tmpAcc];
        end
    end

%============================================================
% Analyse success and failure using svm with labels.
    if (svm_SxF || svm_Fsub)

	    %========================================================
	    % And if using LLBs
	
	    if (svm_LLB_whole + svm_LLB_approach)
		
		    if (svm_LLB_whole == 1)
			    tmpAcc = svmPredict (rootPath, 'LLB', seqNum, 'All', maxCases);	
                acc = [acc; tmpAcc];
		    end
		
		    if (svm_LLB_approach == 1)
			    tmpAcc = svmPredict (rootPath, 'LLB', seqNum, 'Approach', maxCases);	
                acc = [acc; tmpAcc];
		    end
	    end
	    
	    %========================================================
	    % Or using MCs
	
	    if (svm_MC_whole + svm_MC_approach)
	        if (svm_MC_whole == 1)
			    tmpAcc = svmPredict (rootPath, 'MC', seqNum, 'All', maxCases);	
                acc = [acc; tmpAcc];
	        end
	
	        if (svm_MC_approach == 1)
			    tmpAcc = svmPredict (rootPath, 'MC', seqNum, 'Approach', maxCases);	
                acc = [acc; tmpAcc];
	        end
	
	    end
	
	    %========================================================
	    % Or using Ps
	    
	    if (svm_P_whole + svm_P_approach)
	      
	        if (svm_P_whole == 1)
			    tmpAcc = svmPredict (rootPath, 'P', seqNum, 'All', maxCases);	
                acc = [acc; tmpAcc];
	        end

	        if (svm_P_approach == 1)
			    tmpAcc = svmPredict (rootPath, 'P', seqNum, 'Approach', maxCases);	
                acc = [acc; tmpAcc];
	        end
	    end
    end
%============================================================
% Analyse success and failure using logistic regression
% SVMstruct
end
