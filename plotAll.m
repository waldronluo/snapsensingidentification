
function plotAll (rootPath)
    global successPlot;
    global failurePlot;
    global prelimPlot;
    global expPlot;

    successPlot = 0;
    failurePlot = 1;
    prelimPlot = 0;
    expPlot = 0;

    MaxSize = 25;

    addpath ('./svm');
    addpath ('./load');
    
    axis = ['fx'; 'fy'; 'fz'; 'mx'; 'my'; 'mz'];
    cases = ls (rootPath);
    j = 0;
    for i = 1:size(cases, 1)
        if ( ifPlot(cases(i,:)) )
            ++ j;
        end
    end
%    if (j > MaxSize)
%        j = MaxSize;
%    end

    plotsize = ceil(sqrt(j));
    k = 1;

%    for i = 1:size(cases, 1)
%        if (j == MaxSize + 1)
%            continue;
%        end
%
%        if ( ifPlot(cases(i, :)) )
%            forceSig = load (strcat (rootPath, '/', cases(i,:), '/', 'Torques.dat'));
%            state = load (strcat (rootPath, '/', cases(i,:), '/', 'State.dat'));
%            approachSig = forceSig (forceSig(:,1) < state(2), :);
%            for k = 1:6
%                h = figure (k);
%                set (h, 'name', axis(k,:)); 
%                subplot(plotsize, plotsize, j);
%                plot(approachSig(:, 1), approachSig(:, k+1));
%                title(cases(i, :));
%
%            end
%            ++ j;
%        end
%    end
    for i = 1:6
        h = figure(i);
        set (h, 'name', axis(i,:)); 
        k = 1;
        for j = 1:size(cases, 1)
            if (!ifPlot(cases(j, :)))
                continue;
            end
            forceSig = load (strcat (rootPath, '/', cases(j,:), '/', 'Torques.dat'));
            state = load (strcat (rootPath, '/', cases(j,:), '/', 'State.dat'));
            approachSig = forceSig (forceSig(:,1) < state(2), :);
            subplot(plotsize, plotsize, k);
            plot(approachSig(:, 1), approachSig(:, i+1));
            title(cases(k, :));
            k ++;
        end
    end
end

function plotFlag = ifPlot (caseName)

    global successPlot;
    global failurePlot;
    global prelimPlot;
    global expPlot;

    if (strncmp(caseName, 'success', 7) && successPlot == 1)
        plotFlag = 1;
    elseif (strncmp(caseName, 'FC', 2) && failurePlot == 1)
        plotFlag = 1;
    elseif (strncmp(caseName, 'prelim', 6) && prelimPlot == 1)
        plotFlag = 1;
    elseif (strncmp(caseName, 'exp', 3) && expPlot == 1)
        plotFlag == 1;
    else
        plotFlag = 0;
    end
end
