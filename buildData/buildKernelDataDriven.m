
function kernel_Matrix = buildKernelDataDriven (rootPath)
   
   casesName = ls(rootPath);

   cases = [];
   for i = 1:size(casesName, 1)
       caseName = casesName(i,:);
        if (ifBuild(caseName))
            torque = load(strcat(rootPath, '/', caseName, '/', 'Torques.dat'));     
            state = load(strcat(rootPath, '/', caseName, '/', 'State.dat'));   
            torque
            state
            pause
        end
   end



end


function flag = ifBuild(caseStr)
    if (strncmp(caseStr, 'FC', 2) ...
        || strncmp(caseStr, 'prelim', 6) ...
        || strncmp(caseStr, 'success', 7) ...
        || strncmp(caseStr, 'exp', 3))
        flag = 1;
    else
        flag = 0;
    end
end
