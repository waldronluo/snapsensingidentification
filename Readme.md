SVM with RCBHT
==============

###Main files###
The main function here is called analysisCode. It requires two parameters. One compulsory, is the rootPath of the data. In this package, it will be '../..'. The other one is optional, means the number of label sequence we will used. Normally it will set to 1. If you give no value to it, it will automatically set to 1. For our paper, it is one always.  
For the start, I put a start.m in the folder. It trains 100 times and gives accuracy for each. It needs a parameter as the output file name. It will write the accuracy to that file.  
For example, run it like:  
``octave start.m outputFile.dat``  

###Each functions for###
In $PWD  
-   *analysiscode.m* The main function file of the whole analysis.  
-   *plot/* All the files begin with plot are used to show the curves. They are not used in analysiscode.  
-   *buildData*
    -   *buildMCData.m* is used to convert MC from .txt to .mat for speeding up.
    -   *buildPrimitiveData.m* buildMCData.m alike but for Primitive labels.
    -   *MCLabel.m* is used to output the corresponding number of each MC label.
    -   *primitiveLabel.m* MCLabel.m alike but for Primitive labels.
    -   *buildKernelDataDriven.m* Not implemented. Wanted to implement the data-driven method.
-   *load/*
    -   *loadLLBData.m* Used to load LLB labels from $rootPath/CaseName/llBehaviors/.
    -   *loadMCData.m* Used to load LLB labels from $rootPath/CaseName/Composites/.
    -   *loadPData.m* Used to load LLB labels from $rootPath/CaseName/Segments/.
    -   All these data were built previously in buildData. All the data needs to be built by hand. The build Method would not be called in the main script.
-   *svm/*
    -   *libsvm* The files of libsvm contains:
        -   *libsvmread.mex*
        -   *libsvmwrite.mex*
        -   *svmpredict.mex*
        -   *svmtrain.mex*
    -   *svmPredict.m* is directly called by analysiscode.m. It needs the rootpath, the dataType, which is consisted by LLB/MC/P, the seqNum, usually 1, the stage, whether the whole process or the approach state only. This method firstly used the load method to get the apropos data, then give the origin data to svmConstructData.m
    -   *svmConstructData.m* receives the data get used labelsBefore.m to determine if a label should be counted.
    -   *labelSeqBefore.m* to determine if a label or a sequence of label (we do not use sequence of labels here) should be counted. The judgement used the start time of the label, to see if the start time is before the approach state end time. In whole process mode, the end time will be set to 9999s.

###Plot or graphic###
I can not figure out anything that can be pictured ...... Sorry about that.



